Instructions:


1. Clone the source from https://cbgcollabux@bitbucket.org/cbgcollabux/exam.git
    
    git clone https://cbgcollabux@bitbucket.org/cbgcollabux/exam.git

2. Create the database using migrations and follow the database structure below:


table name: bookings
  id 
  date
  customer_id
  cleaner_id
  created_at
  updated_at

table name: cleaners
   id
   first_name
   last_name
   quality_score
   created_at
   updated_at

table name: customers
  id
  first_name 
  last_name
  phone_number
  created_at
  updated_at 

3. Develop CRUD operations for Bookings, Customers and Cleaners